import java.util.LinkedList;
import java.util.Collection;

public class WordSet{
	private static final int TABLE_SIZE = 46;
	private LinkedList[] table = new LinkedList[TABLE_SIZE];
	int base = 39;
	int exponents[];

	/**
	 * Create a WordSet using the given base and exponents
	 */
	public WordSet(int base, int exponents[]){
		this.base = base;
		
		for(int i=0; i<TABLE_SIZE; i++){
			table[i] = new LinkedList<String>();
		}

		this.exponents = new int[exponents.length];
		for(int i=0; i<exponents.length; i++)
			this.exponents[i] = exponents[i];
	}

	/**
	 * Create a WordSet using the given base and exponents 
	 * Store all the strings from words in it
	 */
	public WordSet(int base, int exponents[], String words[]){
		this(base, exponents);

		for(String str: words){
			add(str);
		}
	}

	/**
	 * Create a WordSet using the given and expoents
	 * Store all the strings from words in it
	 */
	public WordSet(int base, int exponents[], Collection<String> words){
		this(base, exponents);

		for(String str: words){
			add(str);
		}
	}

	/**
	 * Generate the hash for the given string
	 */
	public int hash(String word){
		int h = 0;

		for(int i=0; i<word.length(); i++){
			int charVal = word.charAt(i) - 97;
			int coefficient = (int)Math.pow(base, exponents[i]);

			h = h + charVal * coefficient;
		}

		if(h < 0)
			return (TABLE_SIZE + (h % TABLE_SIZE)) % TABLE_SIZE;

		return h % TABLE_SIZE;
	}
	
	/**
	 * add a word to the set
	 */
	public void add(String word){
		int h = hash(word);
		if(!table[h].contains(word))
			table[h].add(word);
	}

	/**
	 * check whether a word is in the set
	 */
	public boolean contains(String word){
		int h = hash(word);

		for(Object o: table[h]){
			if(o.equals(word))
				return true;
		}

		return false;
	}

	/**
	 * print all the words that have collisions
	 */
	public void printCollisions(){
		for(int i=0; i<TABLE_SIZE; i++){
			if(table[i].size() > 1){
				System.out.print(i+": ");
				for(Object o: table[i]){
					System.out.print(o+" ");
				}
				System.out.println();
			}
		}
	}

	/**
	 * display the hash function used
	 */
	public void printHashFunction(){
		System.out.println("hash(w) = SUM w[i] * " + base + "^e[i]");
		System.out.print("e = {");
		
		for(int i=0; i<exponents.length-1; i++){
			System.out.print(exponents[i] + ", "); 
		}

		System.out.println(exponents[exponents.length-1] + "}");

	}

	/**
	 * count and return the number of collisions
	 */
	public int numCollisions(){
		int num = 0;
		for(int i=0; i<TABLE_SIZE; i++){
			if(table[i].size() > 1)
				num += table[i].size() - 1;
		}
		return num;
	}
}
