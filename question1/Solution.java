import java.util.LinkedList;
import java.util.Scanner;
import java.io.FileInputStream;
import java.io.IOException;

public class Solution {
	public static void main(String args[]){
		int exponents[] = {10, 1, 5, 8, 9, 3, 7, 6, 0, 2, 11, 4};
		int base = 13;
		LinkedList<String> words = new LinkedList<String>();
		
		FileInputStream fis = null;

		try{
			fis = new FileInputStream("reservedwords.txt");
		} catch (IOException e) { 
			e.printStackTrace(); 
			System.exit(1); 
		}
		
		Scanner input = new Scanner(fis);

		while(input.hasNext()){
			String line = input.nextLine();
			words.add(line);
		}

		WordSet set = new WordSet(base, exponents, words);

		System.out.println(set.numCollisions() + " collisions");
		set.printHashFunction();
		set.printCollisions();
	}
}
