import java.util.Scanner;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.LinkedList;

public class Tester {

	public static WordSet testExponents(int base, LinkedList<String> words){
		int exponents[] = new int[12];

		for(int i=0; i<12; i++){
			exponents[i] = i;
		}
		
		WordSet bestSet = new WordSet(base, exponents, words);

		for(int tries=0; ; tries++){
			permute(exponents);
			WordSet set = new WordSet(base, exponents, words);
			
			if(set.numCollisions() < bestSet.numCollisions()){
				bestSet = set;
				System.out.println(tries+" attempts");
				System.out.println("Best so far: " + bestSet.numCollisions());
				bestSet.printHashFunction();
			}

			if(bestSet.numCollisions() <= 6)
				return bestSet;

			if(tries % 1000000 == 0)
				System.out.println(tries + " attempts");
		}
	}

	public static void swap(int exponents[], int i, int j){
		int temp = exponents[i];
		exponents[i] = exponents[j];
		exponents[j] = temp;
	}

	public static void permute(int exponents[]){
		int i = (int)(Math.random() * exponents.length);
		int j = (int)(Math.random() * exponents.length);
		swap(exponents, i, j);
	}

	public static void main(String args[]){
		LinkedList<String> words = new LinkedList<String>();
		
		if(args.length < 2){
			System.out.println("Please provide base and filename.");
			System.exit(1);
		}

		int base = Integer.parseInt(args[0]);

		FileInputStream fis = null;

		try{
			fis = new FileInputStream(args[1]);
		} catch (IOException e) { 
			e.printStackTrace(); 
			System.exit(1); 
		}
		
		Scanner input = new Scanner(fis);

		while(input.hasNext()){
			String line = input.nextLine();
			words.add(line);
		}

		WordSet set = testExponents(base, words);

		set.printHashFunction();
		set.printCollisions();
	}
}
