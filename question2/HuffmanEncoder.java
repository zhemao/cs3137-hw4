public class HuffmanEncoder {
	private static final int ASCII_MAX = 127;
	
	private String message;
	private HuffmanQueue queue;
	private HuffmanNode tree;
	private String[] codeTable = new String[ASCII_MAX];
	private int[] frequencies = new int[ASCII_MAX];

	/**
	 * Build an encoder for this message
	 */
	public HuffmanEncoder(String message){
		this.message = message;
		countFrequencies();
		queue = new HuffmanQueue(frequencies);
		tree = buildTree();
		fillCodeTable("", tree);
	}

	/**
	 * Count the frequencies of characters
	 */
	private void countFrequencies(){
		for(int i=0; i<message.length(); i++){
			char c = message.charAt(i);
			frequencies[c]++;
		}
	}

	/**
	 * Build the huffman tree
	 */
	private HuffmanNode buildTree(){
		while(queue.getLength() > 1){
			HuffmanNode left = queue.pop();
			HuffmanNode right = queue.pop();
			HuffmanNode parent = new HuffmanNode(left, right);
			queue.add(parent);
		}
		return queue.getTop();
	}

	/**
	 * Fill the code table with the encoded strings
	 */
	private void fillCodeTable(String accum, HuffmanNode node){
		if(node.isLeaf()){
			codeTable[node.getCharacter()] = accum;
		} else {
			fillCodeTable(accum + "1", node.getLeft());
			fillCodeTable(accum + "0", node.getRight());
		}
	}

	public String getCode(char c){
		return codeTable[c];
	}

	public String toString(){
		String str = "";
		for(int i=0; i<ASCII_MAX; i++){
			if(frequencies[i] != 0){
				str += (char)i + " " + i + " " + frequencies[i] + " " + 
							codeTable[i] + "\n";
			}
		}

		str += message + " " + encode(message);

		return str;
	}

	public HuffmanNode getTree(){
		return tree;
	}

	public String encode(String str){
		String encoded = "";

		for(int i=0; i<str.length(); i++){
			char c = str.charAt(i);
			encoded += codeTable[c];
		} 

		return encoded;
	}
}
