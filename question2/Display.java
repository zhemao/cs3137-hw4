import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JTextArea;
import javax.swing.JLabel;
import java.awt.Dimension;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.LinkedList;

public class Display extends JPanel implements ActionListener {
	
	JTextField messageInput;
	HuffmanEncoder encoder;
	TreePanel tpanel;
	JTextArea textDisplay;

	public Display(){
		setPreferredSize(new Dimension(500, 500));
		
		add(makeButtonPanel());
		
		messageInput = new JTextField();
		messageInput.setPreferredSize(new Dimension(450, 25));
		add(messageInput);

		tpanel = new TreePanel(475, 300);
		add(tpanel);

		textDisplay = new JTextArea();
		textDisplay.setPreferredSize(new Dimension(450, 100));
		add(textDisplay);
	}

	private JPanel makeButtonPanel(){
		String[] commands = {"Build Tree"};
		JPanel panel = new JPanel();

		for(String str: commands){
			JButton button = new JButton(str);
			button.addActionListener(this);
			button.setActionCommand(str);
			panel.add(button);
		}

		return panel;
	}
	
	public static void main(String args[]){
		JFrame frame = new JFrame("HuffmanCode");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setContentPane(new Display());
		frame.pack();
		frame.setVisible(true);
	}

	public void actionPerformed(ActionEvent e){
		encoder = new HuffmanEncoder(messageInput.getText());
		HuffmanNode tree = encoder.getTree();
		tpanel.setTree(tree);
		tpanel.repaint();
		textDisplay.setText(encoder.toString());
	}
}
