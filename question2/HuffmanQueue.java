/**
 * A min-heap containing HuffmanNode objects
 */
public class HuffmanQueue {
	private static final int QUEUE_SIZE = 127;
	private HuffmanNode nodes[];
	private int length;

	public HuffmanQueue(){
		nodes = new HuffmanNode[QUEUE_SIZE+1];
		length = 0;
	}

	/**
	 * Create a new queue with the given frequencies
	 */
	public HuffmanQueue(int frequencies[]){
		this();
		for(char c=0; c < QUEUE_SIZE; c++){
			if(frequencies[c] > 0){
				add(new HuffmanNode(c, frequencies[c]));
			}
		}
	}

	/**
	 * Add a node to the heap
	 */
	public void add(HuffmanNode node){
		int i;

		if(length >= QUEUE_SIZE)
			throw new IndexOutOfBoundsException();
		
		length++;
		
		for(i=length; i > 1; i/=2){
			if(nodes[i/2].compareTo(node) < 0)
				break;
			nodes[i] = nodes[i/2];
		}

		nodes[i] = node;
	}

	public HuffmanNode getTop(){
		return nodes[1];
	}

	/**
	 * pop the node with the smallest frequency off the queue
	 */
	public HuffmanNode pop(){
		int i = 1;
		
		if(length == 0)
			throw new IndexOutOfBoundsException();

		HuffmanNode top = nodes[1];
		HuffmanNode node = nodes[length];
		nodes[length] = null;
		length--;
		
		while(i <= length/2){
			int nextpos;
			if(2*i == length || nodes[2*i].compareTo(nodes[2*i+1]) < 0)
				nextpos = 2*i;
			else nextpos = 2*i+1;

			if(nodes[nextpos].compareTo(node) < 0){
				nodes[i] = nodes[nextpos];
				i = nextpos;
			} else break;
		}

		nodes[i] = node;

		return top;
	}

	public String toString(){
		String str = "";
		for(int i=1; i<=length; i++){
			str += nodes[i] + ", ";
		}
		str += "\n";
		return str;
	}

	public int getLength(){
		return length;
	}
}
