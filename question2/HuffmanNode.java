public class HuffmanNode implements Comparable<HuffmanNode>{
	private char character;
	private int frequency;
	private HuffmanNode left;
	private HuffmanNode right;

	/**
	 * Construct a leaf node with the given character and frequency
	 */
	public HuffmanNode(char c, int f){
		character = c;
		frequency = f;
		left = null;
		right = null;
	}

	/**
	 * Contruct a parent node with the given left and right children
	 */
	public HuffmanNode(HuffmanNode left, HuffmanNode right){
		this.left = left;
		this.right = right;

		character = '-';
		frequency = left.getFrequency() + right.getFrequency();
	}

	public int getFrequency(){
		return frequency;
	}

	public char getCharacter() {
		return character;
	}

	public HuffmanNode getLeft(){
		return left;
	}

	public HuffmanNode getRight(){
		return right;
	}

	public int compareTo(HuffmanNode other){
		return (int)Math.signum(this.getFrequency() - other.getFrequency());
	}

	public String toString(){
		return character + ": " + frequency;
	}

	public boolean isLeaf(){
		return left == null && right == null;
	}

	public int height(){
		if(this.isLeaf())
			return 1;
		
		int lheight = left.height();
		int rheight = right.height();

		return 1 + Math.max(lheight, rheight);
	}

	public int size(){
		if(this.isLeaf())
			return 1;

		return 1 + left.size() + right.size();
	}
}
