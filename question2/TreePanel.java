import javax.swing.JPanel;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;

public class TreePanel extends JPanel {
	private int nodeWidth, nodeHeight;
	private HuffmanNode tree;
	private int index;

	public TreePanel(int width, int height){
		setPreferredSize(new Dimension(width, height));
	}

	public void paintComponent(Graphics g){
		index = 0;
		if(tree != null)
			paintTree(g, "", tree, 0);
	}

	public Point paintTree(Graphics g, String code, 
							HuffmanNode node, int level){
		if(!node.isLeaf()){
			Point leftpos, rightpos;

			leftpos = paintTree(g, code + "1", node.getLeft(), level+1);

			int x = index * nodeWidth;
			int y = level * nodeHeight;
			String str = "-:" + node.getFrequency();

			g.drawOval(x, y, nodeWidth, nodeHeight);
			g.drawString(str, x + nodeWidth / 2 - 5, y + nodeHeight / 2 - 2);

			index += 1;

			rightpos = paintTree(g, code + "0", node.getRight(), level+1);

			g.drawLine(x, y + nodeHeight / 2, 
						leftpos.x + nodeWidth / 2, leftpos.y);
			g.drawLine(x + nodeWidth, y + nodeHeight / 2,
						rightpos.x + nodeWidth / 2, rightpos.y);

			return new Point(x, y);
		} 
		else {
			int x = index * nodeWidth;
			int y = level * nodeHeight;
			String str = node.getCharacter() + ":" + node.getFrequency();

			g.drawOval(x, y, nodeWidth, nodeHeight);
			g.drawString(str, x + nodeWidth / 2 - 5, y + nodeHeight / 2 - 5);
			g.drawString(code, x + nodeWidth / 2 - 5, y + nodeHeight / 2 + 10);
			
			index += 1;

			return new Point(x, y);
		}
	}

	public void setTree(HuffmanNode n){
		nodeWidth = getSize().width / n.size();
		nodeHeight = getSize().height / n.height();
		tree = n;
	}
}
