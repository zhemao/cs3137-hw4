public class Tester {
	public static void main(String args[]){
		if(args.length == 0){
			System.out.println("Usage: java Tester string");
		}

		HuffmanEncoder encoder = new HuffmanEncoder(args[0]);

		for(char c=0; c<127; c++){
			String code = encoder.getCode(c);
			if(code != null){
				System.out.println(c + code);
			}
		}
	}
}
