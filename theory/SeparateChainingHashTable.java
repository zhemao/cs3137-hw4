/**
 * Separate chaining table implementation of hash tables 
 * using Singly-Linked Lists
 * @author Zhehao Mao
 * Based on code from Data Structures and Algorithm Analysis
 * by Mark Allen Weiss
 */
public class SeparateChainingHashTable<T>
{
    class ListNode<V>{
		V value;
		ListNode<V> next;

		public ListNode(V value, ListNode<V> next){
			this.value = value;
			this.next = next;
		}
	}
	
	/**
     * Construct the hash table.
     */
    public SeparateChainingHashTable( )
    {
        this( DEFAULT_TABLE_SIZE );
    }

    /**
     * Construct the hash table.
     * @param size approximate table size.
     */
    public SeparateChainingHashTable( int size )
    {
        theLists = new ListNode[ nextPrime( size ) ];
        for( int i = 0; i < theLists.length; i++ )
            theLists[ i ] = null;
    }

    /**
     * Insert into the hash table. If the item is
     * already present, then do nothing.
     * @param x the item to insert.
     */
    public void insert( T x )
    {
        int h = myhash(x);
		ListNode<T> node = theLists[h];
		ListNode<T> prev = null;
		
		while( node != null){
			if(x.equals(node.value)) 
				return;
			prev = node;
			node = node.next;
		}

		if(prev == null)
			theLists[h] = new ListNode<T>(x, null);
		else prev.next = new ListNode<T>(x, null);

        if( ++currentSize > theLists.length )
        	rehash( );
    }

    /**
     * Remove from the hash table.
     * @param x the item to remove.
     */
    public void remove( T x )
    {
        int h = myhash(x);
		ListNode node = theLists[h];
		ListNode prev = null;

		while(node != null){
			if(x.equals(node.value))
				break;
			prev = node;
			node = node.next;
		}
        
		if( node != null){
            if(prev == null)
				theLists[h] = node.next;
			else
				prev.next = node.next;
			currentSize--;
    	}
    }

    /**
     * Find an item in the hash table.
     * @param x the item to search for.
     * @return true if x isnot found.
     */
    public boolean contains( T x )
    {
		ListNode node = theLists[ myhash(x) ];

		while(node != null){
			if(x.equals(node.value))
				return true;
		}

		return false;
    }

    /**
     * Make the hash table logically empty.
     */
    public void makeEmpty( )
    {
        for( int i = 0; i < theLists.length; i++ )
            theLists[ i ] = null;
        currentSize = 0;    
    }

    /**
     * A hash routine for String objects.
     * @param key the String to hash.
     * @param tableSize the size of the hash table.
     * @return the hash value.
     */
    public static int hash( String key, int tableSize )
    {
        int hashVal = 0;

        for( int i = 0; i < key.length( ); i++ )
            hashVal = 37 * hashVal + key.charAt( i );

        hashVal %= tableSize;
        if( hashVal < 0 )
            hashVal += tableSize;

        return hashVal;
    }

    private void rehash( )
    {
        ListNode[ ]  oldLists = theLists;

            // Create new double-sized, empty table
        theLists = new ListNode[ nextPrime( 2 * theLists.length ) ];

            // Copy table over
        currentSize = 0;
        for(int i=0; i<oldLists.length; i++ ){
			ListNode<T> node = oldLists[i];
			
			while(node != null){
				insert(node.value);
				node = node.next;
			}
		}
    }

    private int myhash( T x )
    {
        int hashVal = x.hashCode( );

        hashVal %= theLists.length;
        if( hashVal < 0 )
            hashVal += theLists.length;

        return hashVal;
    }
    
    private static final int DEFAULT_TABLE_SIZE = 101;

        /** The array of Lists. */
    private ListNode[] theLists; 
    private int currentSize;

    /**
     * Internal method to find a prime number at least as large as n.
     * @param n the starting number (must be positive).
     * @return a prime number larger than or equal to n.
     */
    private static int nextPrime( int n )
    {
        if( n % 2 == 0 )
            n++;

        for( ; !isPrime( n ); n += 2 )
            ;

        return n;
    }

    /**
     * Internal method to test if a number is prime.
     * Not an efficient algorithm.
     * @param n the number to test.
     * @return the result of the test.
     */
    private static boolean isPrime( int n )
    {
        if( n == 2 || n == 3 )
            return true;

        if( n == 1 || n % 2 == 0 )
            return false;

        for( int i = 3; i * i <= n; i += 2 )
            if( n % i == 0 )
                return false;

        return true;
    }


        // Simple main
    public static void main( String [ ] args )
    {
        SeparateChainingHashTable<Integer> H = new SeparateChainingHashTable<Integer>( );

        long startTime = System.currentTimeMillis( );
        
        final int NUMS = 2000000;
        final int GAP  =   37;

        System.out.println( "Checking... (no more output means success)" );

        for( int i = GAP; i != 0; i = ( i + GAP ) % NUMS )
            H.insert( i );
        for( int i = 1; i < NUMS; i+= 2 )
            H.remove( i );

        for( int i = 2; i < NUMS; i+=2 )
            if( !H.contains( i ) )
                System.out.println( "Find fails " + i );

        for( int i = 1; i < NUMS; i+=2 )
        {
            if( H.contains( i ) )
                System.out.println( "OOPS!!! " +  i  );
        }
        
        long endTime = System.currentTimeMillis( );
        
        System.out.println( "Elapsed time: " + (endTime - startTime) );
    }

}

